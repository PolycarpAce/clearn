package com.example.clearn;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class studenlogin extends AppCompatActivity implements View.OnClickListener {

    Button btnLogin;
    EditText etUserName,etPAssord;
    TextView tvRegister;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_studenlogin);
        etUserName = (EditText) findViewById(R.id.etUserName);
        etPAssord = (EditText) findViewById(R.id.etPassword);
        btnLogin = (Button) findViewById(R.id.btnLogin);
        tvRegister = (TextView) findViewById(R.id.tvRegister);

        btnLogin.setOnClickListener(this);
        tvRegister.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnLogin:
                startActivity(new Intent(this,Home.class));

            break;
            case R.id.tvRegister:
                startActivity(new Intent(this, studentregistration.class));
             break;
        }

    }
}
