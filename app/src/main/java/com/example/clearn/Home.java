package com.example.clearn;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class Home extends AppCompatActivity  implements View.OnClickListener{

    Button btnUnitRegister;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        btnUnitRegister = (Button)findViewById(R.id.btnUnitregister);

        btnUnitRegister.setOnClickListener(this);
    }
    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.btnUnitregister:
                startActivity(new Intent(this, UnitRegistretion.class));
                break;
        }

    }
}
