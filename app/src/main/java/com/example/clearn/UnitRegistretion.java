package com.example.clearn;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class UnitRegistretion extends AppCompatActivity implements View.OnClickListener{

    TextView tvUcode;
    EditText etUa, etUb, etUc, etUd, etUe;
    Button btnUregister;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_unit_registretion);

        tvUcode = (TextView) findViewById(R.id.tvUcode);
        etUa = (EditText) findViewById(R.id.etUa);
        etUb = (EditText) findViewById(R.id.etUb);
        etUc = (EditText) findViewById(R.id.etUc);
        etUd = (EditText) findViewById(R.id.etUd);
        etUe = (EditText) findViewById(R.id.etUe);
        btnUregister = (Button) findViewById(R.id.btnUregister);

        btnUregister.setOnClickListener(this);
    }
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnUregister:
                startActivity(new Intent(this, Home.class));
        }

    }
}
