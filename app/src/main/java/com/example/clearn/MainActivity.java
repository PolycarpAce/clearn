package com.example.clearn;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.PopupMenu;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements PopupMenu.OnMenuItemClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void userty(View v){
        PopupMenu popup = new PopupMenu(this, v);
        popup.setOnMenuItemClickListener(this);
        popup.inflate(R.menu.usertype);
        popup.show();
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        switch (item.getItemId()){
            case R.id.student:
                Toast.makeText(this,"Student", Toast.LENGTH_SHORT).show();
                Intent Intent = new Intent(this, studenlogin.class);
                this.startActivity(Intent);


                return  true;

            case R.id.instructor:
                Toast.makeText(this, "Instructor", Toast.LENGTH_SHORT).show();
                return  true;
             default:
                 return false;

        }


    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        switch (item.getItemId()){
            case R.id.student:

                break;
                default:
                    return super.onOptionsItemSelected(item);
        }
        return true;
    }

}
